# Magazine Luiza - Bookmark API

## Execucação:
Na pasta onde foi clonado o repositório, basta executar o comando: `dotnet run --project API`
Para executar a API é necessário ter acesso a um banco de dados PostgreSQL.
Por padrão a string de conexão tem o seguinte formato:
`Server=127.0.0.1; port=5432; user id = postgres; password = adminpassw000rd; database=magalu; pooling = true`
É possível editar esta srintg em [Api/appsettings.json](Api/appsettings.json).
É necessário também ter a porta 50001 liberada para uso que é usada para servir a API. Esta pode ser acessada em https://localhost:5001/swagger/index.html.

## Autenticação e autorização
Por padrão é criado um usuário com permissão de administrador para a criação e atualização de usuários.
```
Login: admin@magalu.com.br
Senha: Supersecretpassw000rd!
```
Todos os usuários posteriores criados terão permissão somente de listagem de clientes e favoritar produtos.
Para qualquer usuário é necessário invocar `vi/account/login` informando o seu email e senha para obter o token de autenticação.
Esse token deve ser enviando no header `Authorization` com o prefixo `Bearer ` para qualquer outra requisição feita. Recomendo usar o botão `Authorize` do botão geral pelo Swagger no link acima para automatizar esse processo.

## Seed de produtos
Para garantir a velocidade da resposta das rotas de favoritos, foi feita a importação dos dados da API de exemplo para o banco local.
A primeira execução do programa pode demorar alguns segundos para que essa importação seja feita. As execuções seguintes checam que esta importação já foi feita e não a realizam novamente.
Um script Python é usado para gerar o SQL que faz esse seed.
Durante esse processo, diversos logs de debug são exibidos.

## Testes:
Os testes rodam sobre o SQLite em memória, então não há dependência externa.
Foram testados operações comuns no qual o sistema se comporta como esperado ("happy paths"), assim como operações operações onde o sistema não se comporta corretamente, em geral por argumentos errados enviados por quem consume a API.

## Melhores práticas
Algumas melhores práticas não foram exatamente seguidas por brevidade no exercício.
A implementação de todas as funcionalidades foram feitas diretamente nos controllers e poderiam ser abstraidas em outros serviços.
A execução dos testes depende de um banco de dados, apesar de agnóstico a esse, porém é possível abstraí-lo.
Também é necessário possuir um servidor `HTTP` pois os testes foram escritos com base nesse protocolo.
Em resumo: os testes feitos foram somente de integração e não unitários. Eu considero estes testes tão efetivos quantos os unitários com o benefício de garantir que uma maior parte do sistema está funcionando. Porém entendo a necessidade de testar unitários em certas partes do sistema.
Espero poder discutir e justificar estas decisões pessoalmente.
