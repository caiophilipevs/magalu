import urllib.request
import json
import codecs

def escape(data: str):
    if not data:
        return 'null'

    data = str(data)
    data = data.replace("'", "''")
    return f"'{ data }'"

if __name__ == '__main__':
    out = codecs.open("../Api/Scripts/database.sql", "w", "utf-8")
    page = 1
    imported = dict()

    while True:
        page = page + 1

        try:
            contents = urllib.request.urlopen(f"http://challenge-api.luizalabs.com/api/product/{page}/").read()
        except urllib.error.HTTPError:
            break
        
        products = json.loads(contents)

        for product in products['products']:
            id = product['id']

            # Avoid reimporting to not break the PK
            if id in imported:
                continue
            imported[id] = True

            id = escape(id)
            title = escape(product['title'])
            image = escape(product['image'])
            price = escape(product['price'])
            reviewscore = escape(product.get('reviewScore'))
            print(f'''insert into "Product" ("Id", "Title", "Image", "Price", "ReviewScore") values ({ id }, { title }, { image }, { price }, { reviewscore });''', file=out)