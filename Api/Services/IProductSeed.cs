using System.Collections.Generic;

namespace magalu.Api.Services
{
    public interface IProductSeed
    {
        IEnumerable<string> SqlSeed();
    }
}