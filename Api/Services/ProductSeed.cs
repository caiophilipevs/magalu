using System.Collections.Generic;
using System.IO;

namespace magalu.Api.Services
{
    public class ProductSeed : IProductSeed
    {
        public IEnumerable<string> SqlSeed()
        {
            var file = Path.Combine(Directory.GetCurrentDirectory(), "Scripts", "database.sql");
            return File.ReadLines(file);
        }
    }
}