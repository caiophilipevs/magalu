using System;

namespace magalu.Api.Services
{
    public class Env : IEnv
    {
        public Guid NewGuid() => Guid.NewGuid();
    }
}