using System;
using Microsoft.AspNetCore.Identity;

namespace magalu.Api.Models
{
    public class Client : IdentityUser<Guid>
    {
        public string Name { get; set; } = null!;
    }
}