using System;

namespace magalu.Api.Models
{
    public class Bookmark
    {
        public Guid ClientId { get; set; }
        public Client Client { get; set; } = null!;

        public Guid ProductId { get; set; }
        public Product Product { get; set; } = null!;

        public DateTime CreatedAt { get; set; }
    }
}